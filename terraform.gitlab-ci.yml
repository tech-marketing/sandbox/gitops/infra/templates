image:
  name: hashicorp/terraform:light
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

# Default output file for Terraform plan
variables:
  PLAN: plan.tfplan
  JSON_PLAN_FILE: tfplan.json
  TF_IN_AUTOMATION: "true"

cache:
  key: "$CI_COMMIT_SHA"
  paths:
    - .terraform

.install-curl-jq: &install-curl-jq
  - apk add --update curl jq
  - alias convert_report="jq -r '([.resource_changes[].change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"

.envconsul: &envconsul
  - curl -s $VAULT_ADDR/v1/sys/seal-status
  - curl -s -O https://releases.hashicorp.com/envconsul/0.9.3/envconsul_0.9.3_linux_amd64.tgz
  - echo "fc48c15851119cf5bd6ecc609b6e2b15ece0c7b4d294c8d1890fddeae36a7aaa  envconsul_0.9.3_linux_amd64.tgz" |sha256sum -c
  - tar -xzvf envconsul_0.9.3_linux_amd64.tgz
  - install envconsul /usr/local/bin && rm envconsul
  - "export VAULT_TOKEN=$(curl -s --request POST --data '{\"jwt\": \"'$CI_JOB_JWT'\"}' $VAULT_ADDR/v1/auth/jwt/login | jq -r '.auth.client_token')"
  - >- 
    if [ -z $VAULT_TOKEN ]; then
      echo "\$VAULT_TOKEN is empty, could be a bad login."
    else
      echo "\$VAULT_TOKEN is NOT empty, attempting to get secrets"
      alias terraform="envconsul -once -config .vault/envconsul.hcl terraform"
    fi

.gitlab-tf-backend: &gitlab-tf-backend
  - export GITLAB_TF_ADDRESS="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${CI_PROJECT_NAME}"
  - echo "Attempting to use backend address $GITLAB_TF_ADDRESS"
  - terraform --version
  - terraform init -reconfigure -backend-config="username=gitlab-ci-token" -backend-config="password=${CI_JOB_TOKEN}"

before_script:
  - *install-curl-jq
#  - *envconsul
  - *gitlab-tf-backend

stages:
  - validate
  - plan
  - apply
  - deploy
  - destroy

validate:
  stage: validate
  script:
    - terraform validate
    - terraform fmt -check=true
  only:
    - branches

merge review:
  stage: plan
  script:
    - terraform show
    - terraform plan -out=$PLAN
    - "terraform show --json $PLAN | convert_report > $JSON_PLAN_FILE"
  artifacts:
    expire_in: 1 week
    name: plan
    reports:
        terraform: $JSON_PLAN_FILE
  only:
    - merge_requests

plan production:
  stage: plan
  script:
    - terraform plan
  only:
    - master
  resource_group: production

apply:
  stage: apply
  script:
    - terraform apply -auto-approve
    - DYNAMIC_ENVIRONMENT_URL=$(terraform output -no-color env-dynamic-url)
    - echo "DYNAMIC_ENVIRONMENT_URL=$DYNAMIC_ENVIRONMENT_URL" >> deploy.env
  dependencies:
    - plan production
  artifacts:
    expire_in: 1 week
    name: $CI_COMMIT_REF_SLUG
    reports:
      dotenv: deploy.env
  only:
    - master
  resource_group: production
  environment:
    name: production
    url: $DYNAMIC_ENVIRONMENT_URL
    on_stop: destroy

deploy-apps:
  stage: deploy
  variables:
    APP_ENVIRONMENT: $CI_PROJECT_NAME
  trigger: tech-marketing/sandbox/gitops/apps/cluster-management
  only:
    - master

destroy:
  stage: destroy
  script:
    - terraform destroy -auto-approve
  when: manual
  only:
    - master  
  environment:
    name: production
    action: stop
